# InfoNegócio Brasil #

### Variáveis para serem criadas no host. ###
* **DEBUG** = True (Default) # Se a aplicação deve ativar o debug ou não
* **COMPRESS_ENABLED** = False (Default) # Se o CSS e JS devem ser concatenados e minificados
* **SITE_DOMAIN** = 'hexxie.com:8000' (Default) # domínio do site, caso hero user o fornecido pelo heroku
