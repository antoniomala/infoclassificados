# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Endereco(models.Model):
    perfil = models.ForeignKey('Profiles')
    cep = models.CharField(max_length=300)
    estado = models.CharField(max_length=300)
    cidade = models.CharField(max_length=300)
    bairro = models.CharField(max_length=300)
    rua = models.CharField(max_length=300, blank=True, null=True)
    numero = models.CharField(max_length=300)

    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'address'


class AnuncioValor(models.Model):
    anuncio = models.ForeignKey('Anuncio')
    valor = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')

    class Meta:
        db_table = 'advert_amounts'


class AnuncioAxeno(models.Model):
    advert = models.ForeignKey('Anuncio')
    image_file = models.CharField(max_length=300, blank=True, null=True)
    image_url = models.CharField(max_length=3000, blank=True, null=True)
    video_file = models.CharField(max_length=300, blank=True, null=True)
    video_url = models.CharField(max_length=3000, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'advert_attachments'


class Anuncio(models.Model):
    user = models.ForeignKey('User')
    subcategory = models.ForeignKey('Subcategoria')
    title = models.CharField(max_length=300, blank=True, null=True)
    slug = models.CharField(max_length=400, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'anuncios'
        verbose_name = u'Anúncio'
        verbose_name_plural = u'Anúncios'


class CartaoDeCredito(models.Model):
    user = models.ForeignKey('User')
    card = models.CharField(max_length=300, blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)

    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'cartoes_de_creditos'
        verbose_name = u'Cartão de credito'
        verbose_name_plural = u'Cartão de credito'


class Categories(models.Model):
    category = models.CharField(max_length=300, blank=True, null=True)
    slug = models.CharField(max_length=350, blank=True, null=True)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'categories'


class CategoryForms(models.Model):
    subcategory = models.ForeignKey('Subcategoria')
    form_title = models.CharField(max_length=300, blank=True, null=True)
    slug = models.CharField(max_length=350, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'category_forms'


class FormSelects(models.Model):
    category_form = models.ForeignKey(CategoryForms)
    self = models.IntegerField(blank=True, null=True)
    select_name = models.CharField(max_length=300, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'form_selects'


class FormTags(models.Model):
    tag = models.ForeignKey('Tags')
    category_form = models.ForeignKey(CategoryForms)

    class Meta:
        db_table = 'form_tags'


class Messagens(models.Model):
    advert = models.ForeignKey(Anuncio)
    from_user = models.ForeignKey('User')
    to_user = models.ForeignKey('User')
    message = models.TextField()
    sending = models.DateTimeField(blank=True, null=True)
    sent = models.DateTimeField(blank=True, null=True)
    received = models.DateTimeField(blank=True, null=True)
    visualized = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'messagens'


class Payments(models.Model):
    user = models.ForeignKey('User')
    plan = models.ForeignKey('Plans')
    advert = models.ForeignKey(Anuncio)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'payments'


class Plans(models.Model):
    plan = models.CharField(max_length=300)
    slug = models.CharField(max_length=350)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    title = models.CharField(max_length=300)
    description = models.TextField()
    image = models.CharField(max_length=300, blank=True, null=True)
    active = models.IntegerField()
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'plans'


class Profiles(models.Model):
    user = models.ForeignKey('User')
    cpf = models.CharField(max_length=300, blank=True, null=True)
    account_type = models.CharField(max_length=300, blank=True, null=True)
    home_phone = models.CharField(max_length=300, blank=True, null=True)
    cell_phone = models.CharField(max_length=300, blank=True, null=True)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'perfils'


class SelectDeOpcoes(models.Model):
    form_select = models.ForeignKey(FormSelects)
    option = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        db_table = 'select_options'
        verbose_name = u'Select de opção'
        verbose_name_plural = u'Select de opções'


class Subcategoria(models.Model):
    subcategory = models.CharField(max_length=300, blank=True, null=True)
    category = models.ForeignKey(Categories)
    slug = models.CharField(max_length=350, blank=True, null=True)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'subcategories'


class Tags(models.Model):
    tag = models.CharField(max_length=300, blank=True, null=True)
    slug = models.CharField(max_length=300, blank=True, null=True)
    
    cadastado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_rm = models.DateTimeField(blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(blank=True, null=True, verbose_name=u'Deletado em')

    class Meta:
        db_table = 'tags'
