# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import *
from apps.mensagem.models import Mensagem, MensagemForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context


@login_required(login_url='/entrar/')
def cadastrar_anuncio(request):
	return render(request, 'cadastrar_anuncio.html')

def ver_anuncio(request, id):
	anuncio = Anuncio.objects.get(pk=id)
	anuncio_titulo = anuncio.titulo
	usuario = User.objects.get(pk=anuncio.usuario_id)
	form = MensagemForm(request.POST or None)
	if form.is_valid():
		form = form.save(commit=False)
		form.usuario = usuario
		form.assunto = anuncio_titulo
		form.save()

		enviou = request.POST['nome']
		assunto = 'Você tem uma nova mensagem CliqueNegócio'
		remetente = 'cliquenegocio@gmail.com'
		para = usuario.email
		text_content = 'Seu anuncio'+ anuncio.titulo +'possue uma nova mensagem'
		htmly = get_template('_email.html')
		variaveis = Context({ 'anuncio': anuncio, 'usuario': usuario, 'enviou': enviou })
		html_content = htmly.render(variaveis)
		msg = EmailMultiAlternatives(assunto, html_content, remetente, [para])
		msg.attach_alternative(html_content, "text/html")
		msg.send()

	return render(request, 'ver_anuncio.html', {'anuncio': anuncio, 'usuario': usuario, 'form': form})

@login_required(login_url='/entrar/')
def anuncio_novo(request, categoria='', subcategoria=''):
	if categoria == 'eletronicos_celulares':
		if subcategoria == 'telefonia':
			form = AnuncioFormCelulareTelefonia(request.POST or None, request.FILES or None)
		elif subcategoria == 'videogames':
			form = AnuncioFormVideogames(request.POST or None, request.FILES or None)
		elif subcategoria == 'computadores':
			form = AnuncioFormComputadores(request.POST or None, request.FILES or None)
		elif subcategoria == 'audio_tv_video_fotografia':
			form = AnuncioFormTvVideoFotografia(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'musica_arte_hobbies':
		if subcategoria == 'instrumentos':
			form = AnuncioFormInstrumentos(request.POST or None, request.FILES or None)
		elif subcategoria == 'musica' or 'livros_revistas' or 'antiguidades' or 'hobbie':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'imoveis':
		if subcategoria == 'apartamentos' or subcategoria == 'casas':
			form = AnuncioFormMoradia(request.POST or None, request.FILES or None)
		elif subcategoria == 'aluguel_quartos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'temporada':
			form = AnuncioFormTemporada(request.POST or None, request.FILES or None)
		elif subcategoria == 'terrenos_lotes':
			form = AnuncioFormTerrenosLotes(request.POST or None, request.FILES or None)
		elif subcategoria == 'lojas_salas':
			form = AnuncioFormLojasSalas(request.POST or None, request.FILES or None)
		elif subcategoria == 'lancamentos':
			form = AnuncioFormLancamentos(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'veiculos':
		if subcategoria == 'motos':
			form = AnuncioFormMotos(request.POST or None, request.FILES or None)
		elif subcategoria == 'carros':
			form = AnuncioFormCarros(request.POST or None, request.FILES or None)
		elif subcategoria == 'caminhoes_onibus':
			form = AnuncioFormCaminhoesOnibus(request.POST or None, request.FILES or None)
		elif subcategoria == 'barcos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'veiculos_pecas_acessorios':
			form = AnuncioFormVeiculosPecas(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'moda_beleza':
		if subcategoria == 'bijout':
			form = AnuncioFormBelezaSaude(request.POST or None, request.FILES or None)
		elif subcategoria == 'roupas_calcados':
			form = AnuncioFormRoupasCalcados(request.POST or None, request.FILES or None)
		elif subcategoria == 'beleza_saude':
			form = AnuncioFormBelezaSaude(request.POST or None, request.FILES or None)
		elif subcategoria == 'bolsas_malas':
			form = AnuncioFormBelezaSaude(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'para_casa':
		if subcategoria == 'moveis':
			form = AnuncioFormMoveis(request.POST or None, request.FILES or None)
		elif subcategoria == 'eletrodomesticos':
			form = AnuncioFormEletrodomesticos(request.POST or None, request.FILES or None)
		elif subcategoria == 'construcao':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'cama_mesa_banho':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'arte_decoracao':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')
	
	elif categoria == 'esportes_fitness':
		if subcategoria == 'esportes':
			form = AnuncioFormEsportes(request.POST or None, request.FILES or None)
		elif subcategoria == 'ciclismo':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'bebes_criancas':
		if subcategoria == 'roupas':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None)
		elif subcategoria == 'brinquedos':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None)
		elif subcategoria == 'carrinhos_cadeiras':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None)
		elif subcategoria == 'bercos_moveis':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None)
		elif subcategoria == 'outros_criancas':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'animais':
		if subcategoria == 'cachorros':
			form = AnuncioFormCachorro(request.POST or None, request.FILES or None)
		elif subcategoria == 'gatos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'roedores':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'cavalos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'peixes':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		elif subcategoria == 'outros_animais':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'empregos_negocios':
		if subcategoria == 'curriculos':
			form = AnuncioFormEmpregoCurriculo(request.POST or None, request.FILES or None)
		elif subcategoria == 'empregos':
			form = AnuncioFormEmpregoCurriculo(request.POST or None, request.FILES or None)
		elif subcategoria == 'servicos':
			form = AnuncioFormServicos(request.POST or None, request.FILES or None)
		elif subcategoria == 'industria':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None)
		else:
			return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/')


	c = categoria
	s = subcategoria


	if form.is_valid():
		form = form.save(commit=False)
		form.usuario = request.user
		form.categoria = request.POST['categoria']
		form.subcategoria = request.POST['subcategoria']
		form.save()
		return HttpResponseRedirect(reverse('home'))

	return render(request, 'anuncio_novo.html', {'form': form, 'request': request, 'categoria': c, 'subcategoria': s})
	
@login_required(login_url='/entrar/')
def editar_anuncio(request, id, categoria='', subcategoria=''):
	instancia = Anuncio.objects.get(pk=id)

	if categoria == 'eletronicos_celulares':
		if subcategoria == 'telefonia':
			form = AnuncioFormCelulareTelefonia(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'videogames':
			form = AnuncioFormVideogames(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'computadores':
			form = AnuncioFormComputadores(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'audio_tv_video_fotografia':
			form = AnuncioFormTvVideoFotografia(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'musica_arte_hobbies':
		if subcategoria == 'instrumentos':
			form = AnuncioFormInstrumentos(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'musica' or 'livros_revistas' or 'antiguidades' or 'hobbie':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'imoveis':
		if subcategoria == 'apartamentos' or subcategoria == 'casas':
			form = AnuncioFormMoradia(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'aluguel_quartos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'temporada':
			form = AnuncioFormTemporada(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'terrenos_lotes':
			form = AnuncioFormTerrenosLotes(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'lojas_salas':
			form = AnuncioFormLojasSalas(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'lancamentos':
			form = AnuncioFormLancamentos(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'veiculos':
		if subcategoria == 'motos':
			form = AnuncioFormMotos(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'carros':
			form = AnuncioFormCarros(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'caminhoes_onibus':
			form = AnuncioFormCaminhoesOnibus(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'barcos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'veiculos_pecas_acessorios':
			form = AnuncioFormVeiculosPecas(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'moda_beleza':
		if subcategoria == 'bijout':
			form = AnuncioFormBelezaSaude(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'roupas_calcados':
			form = AnuncioFormRoupasCalcados(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'beleza_saude':
			form = AnuncioFormBelezaSaude(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'bolsas_malas':
			form = AnuncioFormBelezaSaude(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'para_casa':
		if subcategoria == 'moveis':
			form = AnuncioFormMoveis(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'eletrodomesticos':
			form = AnuncioFormEletrodomesticos(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'construcao':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'cama_mesa_banho':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'arte_decoracao':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')
	
	elif categoria == 'esportes_fitness':
		if subcategoria == 'esportes':
			form = AnuncioFormEsportes(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'ciclismo':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'bebes_criancas':
		if subcategoria == 'roupas':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'brinquedos':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'carrinhos_cadeiras':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'bercos_moveis':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'outros_criancas':
			form = AnuncioFormBebesCriancas(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'animais':
		if subcategoria == 'cachorros':
			form = AnuncioFormCachorro(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'gatos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'roedores':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'cavalos':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'peixes':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'outros_animais':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	elif categoria == 'empregos_negocios':
		if subcategoria == 'curriculos':
			form = AnuncioFormEmpregoCurriculo(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'empregos':
			form = AnuncioFormEmpregoCurriculo(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'servicos':
			form = AnuncioFormServicos(request.POST or None, request.FILES or None, instance=instancia)
		elif subcategoria == 'industria':
			form = AnuncioFormPadrao(request.POST or None, request.FILES or None, instance=instancia)
		else:
			return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/')

	if form.is_valid():
		form = form.save(commit=False)
		form.save()
		return HttpResponseRedirect(reverse('home'))

	return render(request, 'editar_anuncio.html', {'form': form, 'request': request})
