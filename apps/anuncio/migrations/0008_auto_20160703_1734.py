# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anuncio', '0007_anuncio_endereco'),
    ]

    operations = [
        migrations.AddField(
            model_name='endereco',
            name='complemento',
            field=models.CharField(max_length=300, null=True, verbose_name='Complemento', blank=True),
        ),
        migrations.AlterField(
            model_name='endereco',
            name='numero',
            field=models.CharField(max_length=300, verbose_name='Numero'),
        ),
    ]
