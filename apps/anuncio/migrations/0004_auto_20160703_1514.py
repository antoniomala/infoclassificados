# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anuncio', '0003_auto_20160703_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formcategoria',
            name='tags',
            field=models.ManyToManyField(to='anuncio.Tags', verbose_name='Tags'),
        ),
    ]
