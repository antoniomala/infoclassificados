# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('anuncio', '0002_auto_20160703_1426'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formtags',
            name='form_categoria',
        ),
        migrations.RemoveField(
            model_name='formtags',
            name='tag',
        ),
        migrations.AlterModelOptions(
            name='cartaodecredito',
            options={'verbose_name': 'Cart\xe3o de credito', 'verbose_name_plural': 'Cart\xf5es de credito'},
        ),
        migrations.AddField(
            model_name='formcategoria',
            name='tags',
            field=models.ManyToManyField(to='anuncio.Tags', null=True, verbose_name='Tags', blank=True),
        ),
        migrations.AlterField(
            model_name='anunciopago',
            name='anuncio',
            field=models.ForeignKey(related_name='anuncio', to='anuncio.Anuncio'),
        ),
        migrations.AlterField(
            model_name='anunciopago',
            name='usuario',
            field=models.ForeignKey(related_name='usuario', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='FormTags',
        ),
    ]
