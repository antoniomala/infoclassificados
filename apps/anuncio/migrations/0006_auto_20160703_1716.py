# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anuncio', '0005_auto_20160703_1653'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formcategoria',
            name='tags',
        ),
        migrations.AddField(
            model_name='tags',
            name='form_categoria',
            field=models.ForeignKey(default=None, verbose_name='Form categoria', to='anuncio.FormCategoria'),
        ),
    ]
