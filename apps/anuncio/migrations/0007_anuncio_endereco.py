# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anuncio', '0006_auto_20160703_1716'),
    ]

    operations = [
        migrations.AddField(
            model_name='anuncio',
            name='endereco',
            field=models.ForeignKey(verbose_name='Endere\xe7o', blank=True, to='anuncio.Endereco', null=True),
        ),
    ]
