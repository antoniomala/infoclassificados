# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AnexosAnuncio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('arquivo_imagem', models.ImageField(max_length=300, null=True, upload_to=b'', blank=True)),
                ('url_imagem', models.CharField(max_length=3000, null=True, blank=True)),
                ('arquivo_video', models.FileField(max_length=300, null=True, upload_to=b'', blank=True)),
                ('url_video', models.CharField(max_length=3000, null=True, blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
            ],
            options={
                'db_table': 'anexos_anuncios',
                'verbose_name': 'Anexo an\xfancio',
                'verbose_name_plural': 'Anexos an\xfancios',
            },
        ),
        migrations.CreateModel(
            name='Anuncio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=300, null=True, blank=True)),
                ('slug', models.CharField(max_length=400, null=True, blank=True)),
                ('descricao', models.TextField(null=True, blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
            ],
            options={
                'db_table': 'anuncios',
                'verbose_name': 'An\xfancio',
                'verbose_name_plural': 'An\xfancios',
            },
        ),
        migrations.CreateModel(
            name='AnuncioPago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=50, verbose_name='Status', choices=[('Aguardando', 'Aguardando'), ('Aprovado', 'Aprovado'), ('Recusado', 'Recusado')])),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
                ('anuncio', models.ForeignKey(to='anuncio.Anuncio')),
            ],
            options={
                'db_table': 'anuncios_pagos',
                'verbose_name': 'An\xfancio pago',
                'verbose_name_plural': 'An\xfancios pagos',
            },
        ),
        migrations.CreateModel(
            name='CartaoDeCredito',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cartao', models.CharField(max_length=300, null=True, verbose_name='Cart\xe3o', blank=True)),
                ('ativo', models.BooleanField(default=True, verbose_name='Ativo')),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'cartoes_de_creditos',
                'verbose_name': 'Cart\xe3o de credito',
                'verbose_name_plural': 'Cart\xe3o de credito',
            },
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('categoria', models.CharField(max_length=300, null=True, blank=True)),
                ('slug', models.CharField(max_length=350, null=True, blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
            ],
            options={
                'db_table': 'categorias',
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorias',
            },
        ),
        migrations.CreateModel(
            name='Endereco',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cep', models.CharField(max_length=300)),
                ('estado', models.CharField(max_length=300)),
                ('cidade', models.CharField(max_length=300)),
                ('bairro', models.CharField(max_length=300)),
                ('rua', models.CharField(max_length=300, null=True, blank=True)),
                ('numero', models.CharField(max_length=300)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
            ],
            options={
                'db_table': 'enderecos',
                'verbose_name': 'Endere\xe7o',
                'verbose_name_plural': 'Endere\xe7os',
            },
        ),
        migrations.CreateModel(
            name='FormCategoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('form_titulo', models.CharField(max_length=300, null=True, verbose_name='Titulo do formulario', blank=True)),
                ('slug', models.CharField(max_length=350, null=True, blank=True)),
                ('descricao', models.TextField(null=True, verbose_name='Descri\xe7\xe3o', blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
            ],
            options={
                'db_table': 'forms_categorias',
                'verbose_name': 'Formulario para categoria',
                'verbose_name_plural': 'Formularios para categorias',
            },
        ),
        migrations.CreateModel(
            name='FormSelects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('select_name', models.CharField(max_length=300, null=True, verbose_name='Nome do select', blank=True)),
                ('descricao', models.TextField(null=True, verbose_name='Descri\xe7\xe3o', blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
                ('form_categoria', models.ForeignKey(blank=True, to='anuncio.FormCategoria', help_text='Selecione esta op\xe7\xe3o, se o select for para um formulario', null=True, verbose_name='Categoria do form')),
                ('select_pai', models.ForeignKey(blank=True, to='anuncio.FormSelects', help_text='Selecione esta op\xe7\xe3o, se o select \xe9 um filho, caso deva referencia a um outro select', null=True, verbose_name='Select pai')),
            ],
            options={
                'db_table': 'form_selects',
                'verbose_name': 'Form select',
                'verbose_name_plural': 'Form selects',
            },
        ),
        migrations.CreateModel(
            name='FormTags',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('form_categoria', models.ForeignKey(to='anuncio.FormCategoria')),
            ],
            options={
                'db_table': 'form_tags',
                'verbose_name': 'Form tag',
                'verbose_name_plural': 'Form tags',
            },
        ),
        migrations.CreateModel(
            name='Mensagem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mensagem', models.TextField(verbose_name='Mensagem')),
                ('enviando', models.DateTimeField(null=True, verbose_name='Enviando', blank=True)),
                ('enviado', models.DateTimeField(null=True, verbose_name='Enviado', blank=True)),
                ('recebido', models.DateTimeField(null=True, verbose_name='Recebido', blank=True)),
                ('visualizado', models.DateTimeField(null=True, verbose_name='Visualizado', blank=True)),
                ('anuncio', models.ForeignKey(verbose_name='An\xfancio', to='anuncio.Anuncio')),
                ('de', models.ForeignKey(related_name='de', verbose_name='Enviado por', to=settings.AUTH_USER_MODEL)),
                ('para', models.ForeignKey(related_name='para', verbose_name='Enviado pora', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'mensagens',
                'verbose_name': 'Mensagem',
                'verbose_name_plural': 'Mensagens',
            },
        ),
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cpf', models.CharField(max_length=300, null=True, blank=True)),
                ('telefone_residencial', models.CharField(max_length=300, null=True, blank=True)),
                ('telefone_celular', models.CharField(max_length=300, null=True, blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'perfis',
                'verbose_name': 'Perfil',
                'verbose_name_plural': 'Perfis',
            },
        ),
        migrations.CreateModel(
            name='Plano',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plano', models.CharField(max_length=300)),
                ('slug', models.CharField(max_length=350)),
                ('valor', models.DecimalField(max_digits=10, decimal_places=2)),
                ('titulo', models.CharField(max_length=300)),
                ('descricao', models.TextField()),
                ('imagem', models.CharField(max_length=300, null=True, blank=True)),
                ('ativo', models.IntegerField(default=False, verbose_name='Ativo')),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
            ],
            options={
                'db_table': 'planos',
                'verbose_name': 'Plano',
                'verbose_name_plural': 'Planos',
            },
        ),
        migrations.CreateModel(
            name='SelectDeOpcoes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('opcao', models.CharField(max_length=300, null=True, blank=True)),
                ('form_select', models.ForeignKey(to='anuncio.FormSelects')),
            ],
            options={
                'db_table': 'select_opcoes',
                'verbose_name': 'Select de op\xe7\xe3o',
                'verbose_name_plural': 'Select de op\xe7\xf5es',
            },
        ),
        migrations.CreateModel(
            name='Subcategoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subcategoria', models.CharField(max_length=300, null=True, blank=True)),
                ('slug', models.CharField(max_length=350, null=True, blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
                ('categoria', models.ForeignKey(to='anuncio.Categoria')),
            ],
            options={
                'db_table': 'subcategorias',
                'verbose_name': 'Subcategoria',
                'verbose_name_plural': 'Subcategorias',
            },
        ),
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(max_length=300, null=True, blank=True)),
                ('slug', models.CharField(max_length=300, null=True, blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('alterado_em', models.DateTimeField(auto_now=True, verbose_name='Alterado em', null=True)),
                ('deletado_em', models.DateTimeField(null=True, verbose_name='Deletado em', blank=True)),
            ],
            options={
                'db_table': 'tags',
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
        ),
        migrations.CreateModel(
            name='ValoresAnuncio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('valor', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('cadastado_em', models.DateTimeField(auto_now_add=True, verbose_name='Cadastrado em', null=True)),
                ('anuncio', models.ForeignKey(to='anuncio.Anuncio')),
            ],
            options={
                'db_table': 'valores_anuncios',
                'verbose_name': 'Valor an\xfancio',
                'verbose_name_plural': 'Valores an\xfancios',
            },
        ),
        migrations.AddField(
            model_name='formtags',
            name='tag',
            field=models.ForeignKey(to='anuncio.Tags'),
        ),
        migrations.AddField(
            model_name='formcategoria',
            name='subcategoria',
            field=models.ForeignKey(to='anuncio.Subcategoria'),
        ),
        migrations.AddField(
            model_name='endereco',
            name='perfil',
            field=models.ForeignKey(to='anuncio.Perfil'),
        ),
        migrations.AddField(
            model_name='anunciopago',
            name='plano',
            field=models.ForeignKey(to='anuncio.Plano'),
        ),
        migrations.AddField(
            model_name='anunciopago',
            name='usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='anuncio',
            name='subcategoria',
            field=models.ForeignKey(to='anuncio.Subcategoria'),
        ),
        migrations.AddField(
            model_name='anuncio',
            name='usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='anexosanuncio',
            name='anuncio',
            field=models.ForeignKey(to='anuncio.Anuncio'),
        ),
    ]
