# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anuncio', '0004_auto_20160703_1514'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpcoesSelect',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('opcao', models.CharField(max_length=300, null=True, blank=True)),
            ],
            options={
                'db_table': 'opcoes_select',
                'verbose_name': 'Op\xe7\xe3o do select',
                'verbose_name_plural': 'op\xe7\xf5es select',
            },
        ),
        migrations.RemoveField(
            model_name='selectdeopcoes',
            name='form_select',
        ),
        migrations.AlterField(
            model_name='anexosanuncio',
            name='arquivo_imagem',
            field=models.ImageField(max_length=300, null=True, upload_to=b'imagens_anexos_anuncios', blank=True),
        ),
        migrations.AlterField(
            model_name='anexosanuncio',
            name='arquivo_video',
            field=models.FileField(max_length=300, null=True, upload_to=b'arquivos_videos_anexos_anuncios', blank=True),
        ),
        migrations.AlterField(
            model_name='anuncio',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='anuncio',
            name='slug',
            field=models.SlugField(max_length=400, null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='anunciopago',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='anunciopago',
            name='status',
            field=models.CharField(help_text='Status do pagamento.', max_length=50, verbose_name='Status', choices=[('Aguardando', 'Aguardando'), ('Aprovado', 'Aprovado'), ('Recusado', 'Recusado')]),
        ),
        migrations.AlterField(
            model_name='cartaodecredito',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='categoria',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='categoria',
            name='slug',
            field=models.SlugField(max_length=350, null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='endereco',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='formcategoria',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='formcategoria',
            name='slug',
            field=models.SlugField(max_length=350, null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='formselects',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='plano',
            name='ativo',
            field=models.BooleanField(default=False, verbose_name='Ativo'),
        ),
        migrations.AlterField(
            model_name='plano',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='plano',
            name='descricao',
            field=models.TextField(help_text='Descri\xe7\xe3o para ser exibida na explica\xe7\xe3o.', verbose_name='Descri\xe7\xe3o'),
        ),
        migrations.AlterField(
            model_name='plano',
            name='imagem',
            field=models.ImageField(help_text='Imagem para ser exibida na explica\xe7\xe3o.', max_length=300, null=True, upload_to=b'imagens_planos', blank=True),
        ),
        migrations.AlterField(
            model_name='plano',
            name='slug',
            field=models.SlugField(max_length=350, editable=False),
        ),
        migrations.AlterField(
            model_name='plano',
            name='titulo',
            field=models.CharField(help_text='Titulo para ser exibida na explica\xe7\xe3o.', max_length=300),
        ),
        migrations.AlterField(
            model_name='subcategoria',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='subcategoria',
            name='slug',
            field=models.SlugField(max_length=350, null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='tags',
            name='deletado_em',
            field=models.DateTimeField(verbose_name='Deletado em', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='tags',
            name='slug',
            field=models.SlugField(max_length=300, null=True, editable=False, blank=True),
        ),
        migrations.DeleteModel(
            name='SelectDeOpcoes',
        ),
        migrations.AddField(
            model_name='opcoesselect',
            name='form_select',
            field=models.ForeignKey(to='anuncio.FormSelects'),
        ),
    ]
