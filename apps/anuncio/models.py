# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from localflavor.br.br_states import STATE_CHOICES


class Endereco(models.Model):
    perfil = models.ForeignKey('Perfil')
    cep = models.CharField(max_length=300)
    estado = models.CharField(
        max_length=5, choices=STATE_CHOICES, default=None, blank=True, null=True)
    cidade = models.CharField(max_length=300)
    bairro = models.CharField(max_length=300)
    rua = models.CharField(max_length=300, blank=True, null=True)
    numero = models.CharField(verbose_name=u'Numero', max_length=300)
    complemento = models.CharField(
        verbose_name=u'Complemento', max_length=300, blank=True, null=True)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    class Meta:
        db_table = 'enderecos'
        verbose_name = u'Endereço'
        verbose_name_plural = u'Endereços'


class ValoresAnuncio(models.Model):
    anuncio = models.ForeignKey('Anuncio')
    valor = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True)
    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')

    class Meta:
        db_table = 'valores_anuncios'
        verbose_name = u'Valor anúncio'
        verbose_name_plural = u'Valores anúncios'


class AnexosAnuncio(models.Model):
    anuncio = models.ForeignKey('Anuncio')
    arquivo_imagem = models.ImageField(
        upload_to='imagens_anexos_anuncios', max_length=300, blank=True, null=True)
    url_imagem = models.CharField(max_length=3000, blank=True, null=True)
    arquivo_video = models.FileField(
        upload_to='arquivos_videos_anexos_anuncios', max_length=300, blank=True, null=True)
    url_video = models.CharField(max_length=3000, blank=True, null=True)
    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')

    class Meta:
        db_table = 'anexos_anuncios'
        verbose_name = u'Anexo anúncio'
        verbose_name_plural = u'Anexos anúncios'


class Anuncio(models.Model):
    usuario = models.ForeignKey(User)
    subcategoria = models.ForeignKey('Subcategoria')
    titulo = models.CharField(max_length=300, blank=True, null=True)
    slug = models.SlugField(max_length=400, blank=True,
                            null=True, editable=False)
    descricao = models.TextField(blank=True, null=True)
    endereco = models.ForeignKey(
        'Endereco', verbose_name=u'Endereço', blank=True, null=True)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    def save(self, *args, **kwargs):
        super(Anuncio, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.titulo), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()

    class Meta:
        db_table = 'anuncios'
        verbose_name = u'Anúncio'
        verbose_name_plural = u'Anúncios'

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo


class Categoria(models.Model):
    categoria = models.CharField(max_length=300, blank=True, null=True)
    slug = models.SlugField(max_length=350, blank=True,
                            null=True, editable=False)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.categoria)
        super(Categoria, self).save(*args, **kwargs)

    class Meta:
        db_table = 'categorias'
        verbose_name = u'Categoria'
        verbose_name_plural = u'Categorias'

    def __str__(self):
        return self.categoria

    def __unicode__(self):
        return self.categoria


class Tags(models.Model):
    tag = models.CharField(max_length=300, blank=True, null=True)
    slug = models.SlugField(max_length=300, blank=True,
                            null=True, editable=False)
    form_categoria = models.ForeignKey(
        'FormCategoria', verbose_name=u'Form categoria', default=None)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.tag)
        super(Tags, self).save(*args, **kwargs)

    class Meta:
        db_table = 'tags'
        verbose_name = u'Tag'
        verbose_name_plural = u'Tags'

    def __str_(self):
        return self.tag

    def __unicode_(self):
        return self.tag


class FormCategoria(models.Model):
    subcategoria = models.ForeignKey('Subcategoria')
    form_titulo = models.CharField(
        verbose_name=u'Titulo do formulario', max_length=300, blank=True, null=True)
    slug = models.SlugField(max_length=350, blank=True,
                            null=True, editable=False)
    descricao = models.TextField(
        verbose_name=u'Descrição', blank=True, null=True)
    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.form_titulo)
        super(FormCategoria, self).save(*args, **kwargs)

    class Meta:
        db_table = 'forms_categorias'
        verbose_name = u'Formulario para categoria'
        verbose_name_plural = u'Formularios para categorias'

    def __str__(self):
        return self.form_titulo

    def __unicode__(self):
        return self.form_titulo


class FormSelects(models.Model):
    select_name = models.CharField(
        verbose_name=u'Nome do select', max_length=300, blank=True, null=True)
    form_categoria = models.ForeignKey('FormCategoria', verbose_name=u'Categoria do form', blank=True,
                                       null=True, help_text=u'Selecione esta opção, se o select for para um formulario')
    select_pai = models.ForeignKey('self', verbose_name=u'Select pai',  blank=True, null=True,
                                   help_text=u'Selecione esta opção, se o select é um filho, caso deva referencia a um outro select')
    descricao = models.TextField(
        verbose_name=u'Descrição', blank=True, null=True)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    class Meta:
        db_table = 'form_selects'
        verbose_name = u'Form select'
        verbose_name_plural = u'Form selects'

    def __str__(self):
        return self.select_name

    def __unicode__(self):
        return self.select_name


class Mensagem(models.Model):
    anuncio = models.ForeignKey(Anuncio, verbose_name=u'Anúncio')
    de = models.ForeignKey(User, related_name='de',
                           verbose_name=u'Enviado por')
    para = models.ForeignKey(User, related_name='para',
                             verbose_name=u'Enviado pora')
    mensagem = models.TextField(verbose_name=u'Mensagem')
    enviando = models.DateTimeField(
        verbose_name=u'Enviando', blank=True, null=True)
    enviado = models.DateTimeField(
        verbose_name=u'Enviado', blank=True, null=True)
    recebido = models.DateTimeField(
        verbose_name=u'Recebido', blank=True, null=True)
    visualizado = models.DateTimeField(
        verbose_name=u'Visualizado', blank=True, null=True)

    class Meta:
        db_table = 'mensagens'
        verbose_name = u'Mensagem'
        verbose_name_plural = u'Mensagens'

    def __str__(self):
        return "Mensagem de %s para %s" % (self.de.username, self.para.username)

    def __unicode__(self):
        return "Mensagem de %s para %s" % (self.de.username, self.para.username)


class AnuncioPago(models.Model):

    STATUS_ANUNCIO_PAGO_CHOICES = (
        (u'Aguardando', u'Aguardando'),
        (u'Aprovado', u'Aprovado'),
        (u'Recusado', u'Recusado'),
    )

    usuario = models.ForeignKey(User, related_name='usuario')
    plano = models.ForeignKey('Plano')
    anuncio = models.ForeignKey(Anuncio, related_name='anuncio')
    status = models.CharField(verbose_name=u'Status', max_length=50,
                              choices=STATUS_ANUNCIO_PAGO_CHOICES, help_text=u'Status do pagamento.')

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    class Meta:
        db_table = 'anuncios_pagos'
        verbose_name = u'Anúncio pago'
        verbose_name_plural = u'Anúncios pagos'

    def __str__(self):
        return u'Anúnio pago'

    def __unicode__(self):
        return u'Anúnio pago'


class Plano(models.Model):
    plano = models.CharField(max_length=300)
    slug = models.SlugField(max_length=350, editable=False)
    valor = models.DecimalField(max_digits=10, decimal_places=2)
    titulo = models.CharField(
        max_length=300, help_text=u'Titulo para ser exibida na explicação.')
    descricao = models.TextField(
        verbose_name=u'Descrição', help_text=u'Descrição para ser exibida na explicação.')
    imagem = models.ImageField(upload_to='imagens_planos', max_length=300, blank=True, null=True,
                               help_text=u'Imagem para ser exibida na explicação.')
    ativo = models.BooleanField(verbose_name=u'Ativo', default=False)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.plano)
        super(Plano, self).save(*args, **kwargs)

    class Meta:
        db_table = 'planos'
        verbose_name = u'Plano'
        verbose_name_plural = u'Planos'

    def __str__(self):
        return self.plano

    def __unicode__(self):
        return self.plano


class Perfil(models.Model):
    usuario = models.ForeignKey(User)
    cpf = models.CharField(max_length=300, blank=True, null=True)
    telefone_residencial = models.CharField(
        max_length=300, blank=True, null=True)
    telefone_celular = models.CharField(max_length=300, blank=True, null=True)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    class Meta:
        db_table = 'perfis'
        verbose_name = u'Perfil'
        verbose_name_plural = u'Perfis'

    def __str__(self):
        return self.usuario.username

    def __unicode__(self):
        return self.usuario.username


class CartaoDeCredito(models.Model):
    usuario = models.ForeignKey(User)
    cartao = models.CharField(verbose_name=u'Cartão',
                              max_length=300, blank=True, null=True)
    ativo = models.BooleanField(verbose_name=u'Ativo', default=True)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    class Meta:
        db_table = 'cartoes_de_creditos'
        verbose_name = u'Cartão de credito'
        verbose_name_plural = u'Cartões de credito'

    def __str__(self):
        return self.cartao

    def __unicode__(self):
        return self.cartao


class OpcoesSelect(models.Model):
    form_select = models.ForeignKey(FormSelects)
    opcao = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        db_table = 'opcoes_select'
        verbose_name = u'Opção do select'
        verbose_name_plural = u'opções select'

    def __str__(self):
        return self.opcao

    def __unicode__(self):
        return self.opcao


class Subcategoria(models.Model):
    subcategoria = models.CharField(max_length=300, blank=True, null=True)
    categoria = models.ForeignKey('Categoria')
    slug = models.SlugField(max_length=350, blank=True,
                            null=True, editable=False)

    cadastado_em = models.DateTimeField(
        auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    alterado_em = models.DateTimeField(
        auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    deletado_em = models.DateTimeField(
        blank=True, null=True, verbose_name=u'Deletado em', editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.subcategoria)
        super(Subcategoria, self).save(*args, **kwargs)

    class Meta:
        db_table = 'subcategorias'
        verbose_name = u'Subcategoria'
        verbose_name_plural = u'Subcategorias'

    def __str__(self):
        return self.subcategoria

    def __unicode__(self):
        return self.subcategoria
