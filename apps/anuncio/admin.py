from django.contrib import admin

from .models import Anuncio, AnexosAnuncio, ValoresAnuncio, Endereco, Perfil, CartaoDeCredito, Categoria, Subcategoria, AnuncioPago, Plano, Tags, FormCategoria, FormSelects, Mensagem, OpcoesSelect


class EnderecoAdmin(admin.ModelAdmin):
    pass
admin.site.register(Endereco, EnderecoAdmin)


class ValoresAnuncioAdmin(admin.ModelAdmin):
    pass
admin.site.register(ValoresAnuncio, ValoresAnuncioAdmin)


class AnexosAnuncioAdmin(admin.ModelAdmin):
    pass
admin.site.register(AnexosAnuncio, AnexosAnuncioAdmin)


class AnuncioPagoAdmin(admin.ModelAdmin):
    pass
admin.site.register(AnuncioPago, AnuncioPagoAdmin)


class TagsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Tags, TagsAdmin)


class AnuncioAdmin(admin.ModelAdmin):
    pass
admin.site.register(Anuncio, AnuncioAdmin)


class PerfilAdmin(admin.ModelAdmin):
    pass
admin.site.register(Perfil, PerfilAdmin)


class CartaoDeCreditoAdmin(admin.ModelAdmin):
    pass
admin.site.register(CartaoDeCredito, CartaoDeCreditoAdmin)


class CategoriaAdmin(admin.ModelAdmin):
    pass
admin.site.register(Categoria, CategoriaAdmin)


class SubcategoriaAdmin(admin.ModelAdmin):
    pass
admin.site.register(Subcategoria, SubcategoriaAdmin)


class TagsAdminInline(admin.StackedInline):
    model = Tags
    extra = 0


class FormCategoriaAdmin(admin.ModelAdmin):
    inlines = [TagsAdminInline]
admin.site.register(FormCategoria, FormCategoriaAdmin)


class FormSelectsAdmin(admin.ModelAdmin):
    pass
admin.site.register(FormSelects, FormSelectsAdmin)


class OpcoesSelectAdmin(admin.ModelAdmin):
    pass
admin.site.register(OpcoesSelect, OpcoesSelectAdmin)


class MensagemAdmin(admin.ModelAdmin):
    pass
admin.site.register(Mensagem, MensagemAdmin)


class PlanoAdmin(admin.ModelAdmin):
    pass
admin.site.register(Plano, PlanoAdmin)
