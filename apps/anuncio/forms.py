# -*- coding: utf-8 -*-
from django.forms import ModelForm

class AnuncioFormCelulareTelefonia(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['titulo', 'celular_marca', 'novo', 'descricao', 'preco', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'celular_marca': forms.Select(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormVideogames(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['titulo', 'videogame_tipo', 'videogame_modelo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'videogame_tipo': forms.Select(attrs={'class': 'form-control'}),
            'videogame_modelo': forms.Select(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormComputadores(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['titulo', 'computadores_tipo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'computadores_tipo': forms.Select(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormTvVideoFotografia(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['titulo', 'tipo_audio_tv', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'tipo_audio_tv': forms.Select(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormInstrumentos(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['titulo', 'instrumento_tipo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'instrumento_tipo': forms.Select(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormPadrao(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['titulo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormMoradia(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['tipo_oferta', 'numero_quarto', 'area_util', 'vagas_garagem', 'condominio', 'iptu',
                  'ar_condicionado', 'academia', 'armarios', 'varanda', 'area_servicos', 'churrasqueira', 'quarto_empregada', 'piscina', 'salao_festas', 'porteiro',
                  'titulo', 'apartamento_tipo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'tipo_oferta': forms.Select(attrs={'class': 'form-control'}),
            'numero_quarto': forms.Select(attrs={'class': 'form-control'}),
            'area_util': forms.TextInput(attrs={'class': 'input-group'}),
            'vagas_garagem': forms.TextInput(attrs={'class': 'input-group'}),
            'condominio': forms.TextInput(attrs={'class': 'input-group'}),
            'iptu': forms.TextInput(attrs={'class': 'input-group'}),

            #'ar_condicionado' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'academia' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'armarios' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'varanda' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'area_servicos' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'churrasqueira' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'quarto_empregada' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'piscina' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'salao_festas' : forms.CheckboxInput(attrs={'class':'input-group'}),
            #'porteiro' : forms.CheckboxInput(attrs={'class':'input-group'}),

            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'apartamento_tipo': forms.Select(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal',
            'area_util': 'm²',
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormTemporada(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['temporada_tipo', 'numero_quarto', 'area_util', 'vagas_garagem', 'condominio', 'iptu',
                  'aquecimento', 'cafe_manha', 'estacionamento', 'fogao', 'geladeira', 'internet', 'lareira', 'maquina_lavar', 'permitido_animais', 'roupa_cama', 'toalhas', 'tv_cabo', 'ventilador',
                  'titulo', 'apartamento_tipo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'temporada_tipo': forms.Select(attrs={'class': 'form-control'}),
            'tipo_oferta': forms.Select(attrs={'class': 'form-control'}),
            'numero_quarto': forms.Select(attrs={'class': 'form-control'}),
            'area_util': forms.TextInput(attrs={'class': 'input-group'}),
            'vagas_garagem': forms.TextInput(attrs={'class': 'input-group'}),
            'condominio': forms.TextInput(attrs={'class': 'input-group'}),
            'iptu': forms.TextInput(attrs={'class': 'input-group'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'apartamento_tipo': forms.Select(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal',
            'area_util': 'm²',
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormTerrenosLotes(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['tipo_terreno', 'area_util', 'vagas_garagem', 'condominio', 'iptu',
                  'titulo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'tipo_terreno': forms.Select(attrs={'class': 'form-control'}),
            'tipo_oferta': forms.Select(attrs={'class': 'form-control'}),
            'area_util': forms.TextInput(attrs={'class': 'input-group'}),
            'vagas_garagem': forms.TextInput(attrs={'class': 'input-group'}),
            'condominio': forms.TextInput(attrs={'class': 'input-group'}),
            'iptu': forms.TextInput(attrs={'class': 'input-group'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'apartamento_tipo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal',
            'area_util': 'm²',
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormLojasSalas(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['tipo_oferta', 'area_util', 'vagas_garagem', 'condominio', 'iptu',
                  'titulo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'tipo_terreno': forms.Select(attrs={'class': 'form-control'}),
            'tipo_oferta': forms.Select(attrs={'class': 'form-control'}),
            'numero_quarto': forms.Select(attrs={'class': 'form-control'}),
            'area_util': forms.TextInput(attrs={'class': 'input-group'}),
            'vagas_garagem': forms.TextInput(attrs={'class': 'input-group'}),
            'condominio': forms.TextInput(attrs={'class': 'input-group'}),
            'iptu': forms.TextInput(attrs={'class': 'input-group'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'apartamento_tipo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal',
            'area_util': 'm²',
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormLancamentos(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['lancamento_tipo', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'lancamento_tipo': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormMotos(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['marca_moto', 'modelo', 'ano', 'cambio', 'combustivel', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'marca_moto': forms.Select(attrs={'class': 'form-control'}),
            'modelo': forms.TextInput(attrs={'class': 'input-group'}),
            'ano': forms.TextInput(attrs={'class': 'input-group'}),
            'cambio': forms.Select(attrs={'class': 'form-control'}),
            'combustivel': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormCarros(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['marca_carro', 'modelo', 'ano', 'cambio', 'combustivel', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'marca_carro': forms.Select(attrs={'class': 'form-control'}),
            'modelo': forms.TextInput(attrs={'class': 'input-group'}),
            'ano': forms.TextInput(attrs={'class': 'input-group'}),
            'cambio': forms.Select(attrs={'class': 'form-control'}),
            'combustivel': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormVeiculosPecas(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['tipo_peca_veiculo', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'tipo_peca_veiculo': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormCaminhoesOnibus(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['ano', 'combustivel', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'tipo_peca_veiculo': forms.Select(attrs={'class': 'form-control'}),
            'ano': forms.TextInput(attrs={'class': 'form-control'}),
            'combustivel': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormBelezaSaude(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['genero', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormRoupasCalcados(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['roupas_calcados_tipo', 'genero', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'roupas_calcados_tipo': forms.Select(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormMoveis(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['moveis_tipo', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'moveis_tipo': forms.Select(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormEletrodomesticos(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['eletrodomesticos_tipo', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'eletrodomesticos_tipo': forms.Select(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormBebesCriancas(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['genero', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormEsportes(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['esportes_tipo', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'esportes_tipo': forms.Select(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormCachorro(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['raca_caes', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'raca_caes': forms.Select(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormEmpregoCurriculo(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['adm_sec_fin', 'comer_vend', 'eng_arqu_des', 'tel_inf_mul', 'aten_call', 'ban_seg_con_jur',
                  'log_dis', 'tur_hot_res', 'edu_form', 'mar_com', 'serdom_limp', 'cons_ind', 'sau_med_enf', 'agr_pec_vet', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True


class AnuncioFormServicos(ModelForm):

    class Meta:
        model = Anuncio
        fields = ['domesticos', 'outros', 'baba', 'eventos_festas', 'reparacao', 'saude_beleza', 'informatica', 'traducao',
                  'transporte_mudancas', 'liberais', 'turismo', 'titulo', 'novo', 'descricao', 'preco', 'imagem1', 'imagem2', 'imagem3', 'imagem4',
                  'imagem5', 'imagem6', 'cep', 'estado', 'cidade', 'bairro', 'telefone', 'celular', 'whatsapp']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'novo': forms.Select(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
            'cep': forms.TextInput(attrs={'class': 'input-group'}),
            'preco': forms.TextInput(attrs={'class': 'input-group'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
            'cidade': forms.Select(attrs={'class': 'form-control'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'preco': 'use "." para separar os centavos',
            'imagem1': 'imagem principal'
        }

        def __init__(self, *args, **kwargs):
            super(AnuncioFormCelulareTelefonia, self).__init__(*args, **kwargs)
            self.fields['preco'].localize = True
            self.fields['preco'].widget.is_localized = True
