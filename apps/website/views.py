# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from apps.anuncio.models import *
import datetime
from django.core.paginator import Paginator
from localflavor.br.br_states import STATE_CHOICES
from context_processors import get_estado


def home(request):
    estado = get_estado(request).get('estado', None)
    if not estado:
        return render(request, 'mapa.html')

    estados = {k.lower(): v for k, v in STATE_CHOICES}
    if estado == 'brasil':
        todos_anuncios = Anuncio.objects.all().order_by('-created_on')
    else:
        todos_anuncios = Anuncio.objects.all().order_by('-created_on')

    hoje = datetime.datetime.now()

    # cria a paginação configurara para 25 por página
    paginator = Paginator(todos_anuncios, 25)
    # Pega a pagina informada na url ou define como primeira pagina
    page = request.GET.get('page', 1)
    anuncios = paginator.page(page)

    if estado in estados.keys() or estado == 'brasil':
        capital = estados.get(estado, estado.upper())
        return render(request, 'estado.html', {
            'estado': estado.upper(),
            'capital': capital,
            'anuncios': anuncios,
            'hoje': hoje
        })
    else:
        return HttpResponseRedirect('/')
