from localflavor.br.br_states import STATE_CHOICES
from infonegocio.settings import env


def get_estado(request):
    estados = [uf.lower() for uf, estado in STATE_CHOICES]
    host = env('SITE_DOMAIN', 'hexxie.com:8000')
    host_exploded = request.META['HTTP_HOST'].split('.')
    estado = None
    if 'www' in host_exploded:
        estado = host_exploded[1]
    else:
        estado = host_exploded[0]

    if estado not in estados and estado != 'brasil':
        estado = None

    return {
        'estado': estado,
        'endereco_site': host
    }
