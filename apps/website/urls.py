# -*- coding: utf-8 -*-
from django.conf.urls import url
from apps.website import views

urlpatterns = [
    # navegação por estados/cidades
    url(r'^$', views.home, name='home'),
    # url(r'^(?P<cidade>.*)-(?P<estado>\w+)/$', 'apps.home.views.cidade', name='cidade'),
    # url(r'^(?P<cidade>.*)-(?P<estado>\w+)/(?P<categoria>\w+)/$', 'apps.home.views.cidade_categoria', name='cidade_categoria'),
    # url(r'^(?P<cidade>.*)-(?P<estado>\w+)/(?P<categoria>\w+)/(?P<subcategoria>\w+)/$', 'apps.home.views.cidade_subcategoria', name='cidade_subcategoria'),
    # url(r'^(?P<estado>\w+)/$', 'apps.home.views.estado', name='estado'),
    # url(r'^(?P<estado>\w+)/(?P<categoria>\w+)/$', 'apps.home.views.estado_categoria', name='estado_categoria'),
    # url(r'^(?P<estado>\w+)/(?P<categoria>\w+)/(?P<subcategoria>\w+)/$', 'apps.home.views.estado_subcategoria', name='estado_subcategoria'),
]
