# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.forms import ModelForm
from django.contrib.auth.models import User
from django import forms

class Mensagem(models.Model):
	usuario = models.ForeignKey(User)
	nome = models.CharField(u'', max_length=50)
	assunto = models.CharField(u'', max_length=100)
	email = models.EmailField(u'')
	telefone = models.CharField(u'', max_length=50, blank=True, null=True)
	corpo = models.TextField(u'')
	lido = models.BooleanField(default=False)
	created_on = models.DateTimeField(auto_now_add=True)

class MensagemForm(ModelForm):
	class Meta:
		model = Mensagem
		fields = ['nome', 'email', 'telefone', 'corpo']

		widgets = {
			'nome' : forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Nome'}),	
			'email' : forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Email'}),
			'telefone' : forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Telefone'}),
			'corpo' : forms.Textarea(attrs = {'class': 'form-control', 'placeholder': 'Mensagem', 'rows': 7}),
		}
		