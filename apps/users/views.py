# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import *
from apps.anuncio.models import Anuncio
from apps.mensagem.models import Mensagem
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

def cadastre_se(request):
	form = CadastrarUsuarioForm(request.POST or None)

	if form.is_valid():
		form.save()
		return HttpResponseRedirect(reverse('home'))
	
	return render(request, 'cadastre_se.html', {'form': form})

# login no sistema
def entrar(request):
	# checando se o usuário já realizou o login e tomanda a decisão adequada
	if request.user.is_authenticated():
		if request.user.is_superuser:
			return HttpResponseRedirect(reverse('administracao'))
		else:
			return HttpResponseRedirect(reverse('home'))


	form_login = EntrarForm(request.POST or None)
	erro = False

	if request.method == 'POST':
		usuario = authenticate(username=request.POST['username'], password=request.POST['password'])

		if usuario is not None and usuario.is_active:
			login(request, usuario)
			
			# redirecionar para local adequado
			if usuario.is_superuser:
				return HttpResponseRedirect('/painel_adm/')
			else:
				# redireciona para área do cliente
				return HttpResponseRedirect('/painel_usuario/')

		else: erro = True
	
	return render(request, 'entrar.html', {'form_login': form_login, 'erro': erro})

@login_required(login_url='/entrar/')
def painel_usuario(request):
	usuario = request.user
	anuncios = Anuncio.objects.filter(usuario_id=usuario).exclude(status='inativo').order_by('-created_on')
	qtd_total = Anuncio.objects.filter(usuario_id=usuario).exclude(status='inativo').count()
	qtd_pendentes = Anuncio.objects.filter(usuario_id=usuario).filter(status='aguardando').count()
	qtd_ativos = Anuncio.objects.filter(usuario_id=usuario).filter(status='ativo').count()
	titulo = 'Meus Anuncios'
	return render(request, 'painel_usuario.html', {'titulo': titulo, 'usuario': usuario, 'anuncios': anuncios, 'qtd_ativos': qtd_ativos, 'qtd_total': qtd_total, 'qtd_pendentes': qtd_pendentes})

@login_required(login_url='/entrar/')
def painel_usuario_pendentes(request):
	usuario = request.user
	mensagens = Mensagem.objects.filter(usuario_id=usuario)
	qtd_mensagens = mensagens.count()
	anuncios = Anuncio.objects.filter(usuario_id=usuario).filter(status='aguardando').order_by('-created_on')
	qtd_total = Anuncio.objects.filter(usuario_id=usuario).exclude(status='inativo').count()
	qtd_pendentes = Anuncio.objects.filter(usuario_id=usuario).filter(status='aguardando').count()
	qtd_ativos = Anuncio.objects.filter(usuario_id=usuario).filter(status='ativo').count()
	titulo = 'Anuncios Pendentes'
	return render(request, 'painel_usuario.html', {'titulo': titulo, 'usuario': usuario, 'anuncios': anuncios, 'qtd_ativos': qtd_ativos, 'qtd_total': qtd_total, 'qtd_pendentes': qtd_pendentes, 'qtd_mensagens': qtd_mensagens})

@login_required(login_url='/entrar/')
def painel_usuario_publicados(request):
	usuario = request.user
	mensagens = Mensagem.objects.filter(usuario_id=usuario)
	qtd_mensagens = mensagens.count()
	anuncios = Anuncio.objects.filter(usuario_id=usuario).filter(status='ativo').order_by('-created_on')
	qtd_total = Anuncio.objects.filter(usuario_id=usuario).exclude(status='inativo').count()
	qtd_pendentes = Anuncio.objects.filter(usuario_id=usuario).filter(status='aguardando').count()
	qtd_ativos = Anuncio.objects.filter(usuario_id=usuario).filter(status='ativo').count()
	titulo = 'Anuncios Publicados'
	return render(request, 'painel_usuario.html', {'titulo': titulo, 'usuario': usuario, 'anuncios': anuncios, 'qtd_ativos': qtd_ativos, 'qtd_total': qtd_total, 'qtd_pendentes': qtd_pendentes, 'qtd_mensagens': qtd_mensagens})

@login_required(login_url='/entrar/')
def painel_usuario_mensagens(request):
	usuario = request.user
	mensagens = Mensagem.objects.filter(usuario_id=usuario)
	qtd_mensagens = mensagens.count()
	anuncios = Anuncio.objects.filter(usuario_id=usuario).filter(status='ativo').order_by('-created_on')
	qtd_total = Anuncio.objects.filter(usuario_id=usuario).exclude(status='inativo').count()
	qtd_pendentes = Anuncio.objects.filter(usuario_id=usuario).filter(status='aguardando').count()
	qtd_ativos = Anuncio.objects.filter(usuario_id=usuario).filter(status='ativo').count()
	titulo = 'Mensagens'
	return render(request, 'painel_usuario.html', {'mensagens': mensagens, 'qtd_mensagens': qtd_mensagens, 'titulo': titulo, 'usuario': usuario, 'anuncios': anuncios, 'qtd_ativos': qtd_ativos, 'qtd_total': qtd_total, 'qtd_pendentes': qtd_pendentes})


@user_passes_test(lambda u: u.is_superuser, login_url='/entrar/')
def painel_adm(request):
	usuario = request.user
	mensagens = Mensagem.objects.filter(usuario_id=usuario)
	qtd_mensagens = mensagens.count()
	anuncios = Anuncio.objects.all()
	qtd_total = anuncios.count()
	qtd_pendentes = Anuncio.objects.filter(status='aguardando').count()
	qtd_ativos = Anuncio.objects.filter(status='ativo').count()
	titulo = 'Anuncios'
	return render(request, 'painel_adm.html', {'titulo': titulo, 'anuncios': anuncios, 'qtd_ativos': qtd_ativos, 'qtd_total': qtd_total, 'qtd_pendentes': qtd_pendentes, 'qtd_mensagens': qtd_mensagens})

@user_passes_test(lambda u: u.is_superuser, login_url='/entrar/')
def painel_adm_usuarios(request):
	usuario = request.user
	mensagens = Mensagem.objects.filter(usuario_id=usuario)
	qtd_mensagens = mensagens.count()
	usuarios = User.objects.all()
	usuarios_inativos = usuarios.filter(is_active=False).count()
	usuarios_ativos = usuarios.filter(is_active=True).count()
	usuarios_total = usuarios.count()

	return render(request, 'painel_adm_usuarios.html', {'usuarios': usuarios, 'usuarios_total': usuarios_total, 'usuarios_inativos': usuarios_inativos, 'usuarios_ativos': usuarios_ativos, 'qtd_mensagens': qtd_mensagens})

# logout no sistema
def sair(request):
	logout(request)
	return HttpResponseRedirect('/')