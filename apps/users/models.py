# -*- coding: utf-8 -*-
from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

class CadastrarUsuarioForm(UserCreationForm):

	class Meta:
		model = User
		fields = ['username', 'email']
		widgets = {
			'username': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Username'}),
			'first_name': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Username'}),
			'last_name': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'Username'}),
			'email': forms.TextInput(attrs = {'class': 'form-control', 'placeholder': 'email'}),
		}
	def __init__(self, *args, **kwargs):
		super(CadastrarUsuarioForm, self).__init__(*args, **kwargs)
		self.fields['username'].help_text=''
		self.fields['username'].label=' '
		self.fields['email'].label=''
		self.fields['password1'].label=''
		self.fields['password2'].label=''
		self.fields['email'].required = True
		self.fields['password1'].widget = forms.PasswordInput(attrs={
			'class': 'form-control',
			'placeholder': 'Digite sua senha'})
		self.fields['password2'].widget = forms.PasswordInput(attrs={
			'class': 'form-control',
			'placeholder': 'Repita sua senha'})
		self.fields['password2'].help_text=''

	def clean_email(self):
		data = self.cleaned_data['email']
		if User.objects.filter(email=data).exists():
			raise forms.ValidationError("Email já cadastrado no sistema.")
		return data

	def clean_password1(self):
		data = self.cleaned_data['password1']
		if len(data) < 6:
			raise forms.ValidationError("Password precisa ter ao menos 6 caracteres.")
		return data

class EntrarForm(AuthenticationForm):
	class Meta:
		model = User
		exclude = []

	def __init__(self, *args, **kwargs):
		super(EntrarForm, self).__init__(*args, **kwargs)
		self.fields['username'].widget = forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'usuario ou email'})

		self.fields['password'].widget = forms.PasswordInput(attrs={
			'class': 'form-control',
			'placeholder': 'Digite sua senha'})
