# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # Examples:
    # url(r'^$', 'infonegocio.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^cadastre_se/$', 'apps.users.views.cadastre_se', name='cadastre_se'),
	url(r'^entrar/$', 'apps.users.views.entrar', name='entrar'),

	url(r'^painel_adm/$', 'apps.users.views.painel_adm', name='painel_adm'),
	url(r'^painel_adm/usuarios$', 'apps.users.views.painel_adm_usuarios', name='painel_adm_usuarios'),

	url(r'^painel_usuario/$', 'apps.users.views.painel_usuario', name='painel_usuario'),
	url(r'^painel_usuario/pendentes/$', 'apps.users.views.painel_usuario_pendentes', name='painel_usuario_pendentes'),
	url(r'^painel_usuario/publicados/$', 'apps.users.views.painel_usuario_publicados', name='painel_usuario_publicados'),
	url(r'^painel_usuario/mensagens/$', 'apps.users.views.painel_usuario_mensagens', name='painel_usuario_mensagens'),
	
	url(r'^sair/$', 'apps.users.views.sair', name='sair'),

	# url(r'^anuncio/cadastrar_anuncio/$', 'apps.anuncio.views.cadastrar_anuncio', name='cadastrar_anuncio'),
	# url(r'^anuncio/editar_anuncio/(?P<categoria>\w+)/(?P<subcategoria>\w+)/(?P<id>\d+)/$', 'apps.anuncio.views.editar_anuncio', name='editar_anuncio'),
	# url(r'^anuncio/ver_anuncio/(?P<id>\d+)/$', 'apps.anuncio.views.ver_anuncio', name='ver_anuncio'),
	# url(r'^anuncio/(?P<categoria>\w+)/(?P<subcategoria>\w+)/$', 'apps.anuncio.views.anuncio_novo', name='anuncio_novo'),
	# url(r'^anuncio_novo/(?P<categoria>\w+)/$', 'apps.anuncio.views.anuncio_novo', name='anuncio_novo'),
	# url(r'^anuncio_novo/(?P<categoria>\w+)/(?P<subcategoria>\w+)/$', 'apps.anuncio.views.anuncio_novo', name='anuncio_novo'),

    url(r'^admin/', include(admin.site.urls)),


	url(r'^$', include('apps.website.urls')),
	# anuncio

	# habilitando a visualização dentro dos arquivos em MEDIA - não usar em produção
	url(r'^media/(.*)/$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
