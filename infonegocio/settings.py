# -*- coding: utf-8 -*-
from unipath import Path
import dj_database_url
from django.conf.global_settings import STATICFILES_FINDERS
BASE_DIR = Path(__file__).ancestor(2)


def env(env_name, default_val=None):
    import os
    return os.environ.get(env_name, default_val)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ngi9by8yem%!%6^vjn(66m29lfq$pa(^m)jrx_foogk131gb^j'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env('DEBUG', True)

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    # django core apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # external apps
    'imagekit',
    'gunicorn',
    'compressor',
    # apps
    'apps.users',
    'apps.website',
    'apps.anuncio',
    'apps.mensagem',
)

AUTHENTICATION_BACKENDS = (
    'apps.users.backends.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'infonegocio.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR.child('templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'apps.website.context_processors.get_estado'
            ],
        },
    },
]

WSGI_APPLICATION = 'infonegocio.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

# Mailer conf
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'cliquenegocio@gmail.com'
EMAIL_HOST_PASSWORD = 'clique@negocio'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

STATICFILES_FINDERS += ('compressor.finders.CompressorFinder',)


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'pt-BR'

TIME_ZONE = 'America/Recife'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DECIMAL_SEPARATOR = ','

# setting session to timeout
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR.child('media')

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR.child('static')


# Django compressor settings
COMPRESS_ENABLED = env('COMPRESS_ENABLED', False)

COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.rCSSMinFilter',
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter',
]

if DEBUG is False:
    DATABASES = {'default': dj_database_url.config(
        default='postgres://kzxhmaziwahbdo:sKPNEw3hMM2MvUln0Z8lQLdkZX@ec2-54-235-246-67.compute-1.amazonaws.com:5432/dd1h81540e838k')}

# Trying to load local settings
try:
    execfile(BASE_DIR.child('infonegocio', 'settings_local.py'),
             globals(), locals())
except IOError:
    pass
